import React, {useState , useEffect} from 'react';
import {BehaviorSubject , from} from 'rxjs'
import {map} from 'rxjs/operators'
import {Button} from '@material-ui/core'

const ButtonInput:React.FC<{inputText : BehaviorSubject<string> , list : BehaviorSubject<string[]> , editStatus : BehaviorSubject<boolean> , editItem : BehaviorSubject<string>}> = ({
    inputText , list , editStatus , editItem
})=>{

    const [edit , setEditStatus] = useState<boolean>(false)

    let buttonValue = editStatus.value ? 'EDIT' : 'ADD'

    useEffect(() => {
        const editSub = editStatus.subscribe(status => setEditStatus(status))
        return () => {
            editSub.unsubscribe()
        }
    }, [])

    const addTaskHandler = ()=>{
        if (inputText.value) {
            list.next([...list.value].concat([inputText.value]));
            inputText.next("");
          } else {
            alert("please enter task title");
          }
    }


    const editTaskHandler = () =>{
      const newList = list.value.map((value ) =>{
            if(value === editItem.value){
            value = inputText.value
            }
            return value
        })

        list.next(newList)
        inputText.next("")
        editStatus.next(false)
    }
    return (
       <>
             <Button
              color="primary"
              size="large"
              style={{
                padding: "15px",
                marginLeft: "7px",
              }}
              variant="contained"
              onClick={editStatus.value ? editTaskHandler : addTaskHandler}
            >
            {buttonValue}
            </Button>
            {editStatus.value ? 
            <Button
                 color="secondary"
              size="large"
              style={{
                padding: "15px",
                marginLeft: "7px",
              }}
              variant="contained" 
              onClick={() => {
                editStatus.next(false)
                inputText.next('')
              }}
              >
                CA
            </Button> : null }
       </>
    )
}
export default ButtonInput