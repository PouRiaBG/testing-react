import { InputAdornment, TextField } from "@material-ui/core";
import React, { ReactNode, useEffect, useState } from "react";
import { BehaviorSubject } from "rxjs";
interface Props {
  value: BehaviorSubject<string>;
  icon: ReactNode;
}
const TextInput = (props: Props) => {
  const [value, setValue] = useState("");

  useEffect(() => {
    const valueSub = props.value.subscribe((i) => {
      setValue(i);
    });
    return () => {
      valueSub.unsubscribe();
    };
  }, []);
  
  return (
    <TextField
      variant="outlined"
      label="User name"
      color="primary"
      name="user"
      value={value}
      InputProps={{
        endAdornment: (
          <InputAdornment position="start">{props.icon}</InputAdornment>
        ),
      }}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
        props.value.next(event.target.value)
      }
    />
  );
};
export default TextInput;
