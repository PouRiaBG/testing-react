import { Grid, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LockIcon from "@material-ui/icons/Lock";
import PersonIcon from "@material-ui/icons/Person";
import React from "react";
import { BehaviorSubject, combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import ButtonInput from "./Test";
import TextInput from "./TextInput";

const useStyle = makeStyles({
  root: {
    height: "100vh",
  },
  paper: {
    height: "450px",
    width: "400px",
    display: "flex",
    flexDirection: "column",
  },
  buuton: {
    width: "140px",
    height: "50px",
  },
});

const Login = () => {
  // //custom classs
  const classes = useStyle();
  const userName = new BehaviorSubject<string>("");
  const password = new BehaviorSubject<string>("");


  return (
    <>
      <Grid
        container
        item
        lg={12}
        justify="center"
        alignItems="center"
        className={classes.root}
      >
        <Paper elevation={3} className={classes.paper}>
          <Grid item lg={12} container justify="center" alignItems="center">
            <Typography variant="h4" color="primary">
              Welcome..!
            </Typography>
          </Grid>

          <Grid item lg={12} justify="center" container>
            <TextInput value={userName} icon={<PersonIcon />} />
          </Grid>

          <Grid item lg={12} container justify="center">
            <TextInput value={password} icon={<LockIcon />} />
          </Grid>

          <Grid item lg={12} container justify="center">
            <ButtonInput
              disable={combineLatest(userName , password).pipe(
                map(([u, p]) => !!!u || !!!p)
              )}
            />
          </Grid>
        </Paper>
      </Grid>
    </>
  );
};
export default Login;
