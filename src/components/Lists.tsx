import { makeStyles, Paper, Typography , Fade } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { BehaviorSubject } from "rxjs";
import {Button } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton } from '@material-ui/core';
import DeleteOutlineRoundedIcon from '@material-ui/icons/DeleteOutlineRounded';
const useStyle = makeStyles({
  paper: {
    margin: "10px",
    width: '350px',
    height: '70px',
    display : 'flex',
    alignItems: "center",
    justifyContent: 'space-evenly',
    transition : 'all 3s'
  },
});
const Lists: React.FC<{ list: BehaviorSubject<string[]> , editStatus:BehaviorSubject<boolean>, editItem : BehaviorSubject<string>}> = ({ list , editStatus ,editItem}) => {
  const classes = useStyle();
  const [data, setData] = useState<string[]>([]);

  useEffect(() => {
    const dataSub = list.subscribe((i) => setData(i));
    return () => {
      dataSub.unsubscribe();
    };
  }, []);

  console.log(list.value);

  const deleteItem = (item : string)=>{
    const newList = list.value.filter(value => value !== item) 
    console.log(newList)
    list.next(newList)
  }

  const editItemHandler = (item : string)=>{
    editStatus.next(true)
    editItem.next(item)

  }
  return (
    <>
      {data.map((value) => (
       <Fade in={true} timeout={1000}>
           <Paper elevation={3} className={classes.paper}>
            <ul>
                <Typography variant="h5">
                  <li style={{listStyleType: 'none', color : 'black'}} key={value}>{value}</li> 
                </Typography>
            </ul>
            {/* Delete Button */}
            <IconButton color="secondary">
              <DeleteOutlineRoundedIcon
              onClick={() => deleteItem(value)}
              />
            </IconButton>
            {/* Edit Button */}
            <IconButton>
                <EditIcon
                color="primary"
                onClick={()=> editItemHandler(value)}
                />
            </IconButton>
         </Paper>
       </Fade>
      ))}
    </>
  );
};

export default Lists;

