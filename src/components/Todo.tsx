import { Button, Grid, makeStyles , Paper,} from "@material-ui/core";
import React from "react";
import { BehaviorSubject } from "rxjs";
import InputField from "./InputField";
import Lists from "./Lists";
import ButtonInput from './ButtonInput'

const useStyle = makeStyles({
  root: {
    height: "100vh",
  },
  grid1: {
    width: "400px",
    height: "400px",
  },
  grid2: {
    backgroundColor: "white",
  },
  paper2 : {
    width: '350px',
    height: '80px',
    display : 'flex',
    alignItems: "center",
    justifyContent: 'space-evenly'
  }
});

const Todo: React.FC = () => {
  const inputText: BehaviorSubject<string> = new BehaviorSubject("");
  const list: BehaviorSubject<string[]> = new BehaviorSubject([]);
  const editStatus:BehaviorSubject<boolean> = new BehaviorSubject(false)
  const mustBeEditItem:BehaviorSubject<string> = new BehaviorSubject("")

  const classes = useStyle();

  return (
    <>
      <Grid
        className={classes.root}
        justify="center"
        alignItems="center"
        item
        lg={12}
        container
      >
        <Grid
          item
          container
          lg={4}
          justify="center"
          alignItems="center"
          direction="column"
        >
          <Grid item className={classes.grid2}>
            <Paper className={classes.paper2}
             elevation={3}>
              <InputField editItem={mustBeEditItem}
                value={inputText}
                editStatus={editStatus}/>
                
                <ButtonInput  editItem={mustBeEditItem}
                editStatus={editStatus}
                list={list} 
                inputText={inputText}/>
            </Paper>
          </Grid>
          <Grid
            item
            container
            justify="center"
            direction="column"
            alignItems="center"
            lg={12}
          >
            <Lists list={list} 
            editItem={mustBeEditItem}
            editStatus={editStatus}/>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
export default Todo;
