import { Button, makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Observable } from "rxjs";
interface Props {
  disable: Observable<boolean>;
}
const useStyle = makeStyles({
  root: {
    height: "100vh",
  },
  paper: {
    height: "450px",
    width: "400px",
    display: "flex",
    flexDirection: "column",
  },
  buuton: {
    width: "140px",
    height: "50px",
  },
});

const ButtonInput = (props: Props) => {
  const classes = useStyle();

  const [disable, setDisable] = useState(true);
  
  useEffect(() => {
    const valueSub = props.disable.subscribe((i) => {
      setDisable(i);
    });
    return () => {
      valueSub.unsubscribe();
    };
  }, []);

  return (
    <Button
      className={classes.buuton}
      size="medium"
      variant="contained"
      disabled={disable}
      color="primary"
    >
      LOGIN
    </Button>
  );
};
export default ButtonInput;
