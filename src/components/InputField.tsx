import { TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { BehaviorSubject } from "rxjs";
import {Paper ,makeStyles} from '@material-ui/core'



const InputField: React.FC<{ value: BehaviorSubject<string>  , editStatus : BehaviorSubject<boolean>, editItem : BehaviorSubject<string>}> = ({
  value, editStatus , editItem
}) => {
  const [current, setCurrent] = useState("");
  const [currentEdit , setCurrentEdit] = useState('')

  const [edit , setEditStatus] = useState(false)
   let labeInput:string = editStatus.value ? 'EDIT TASK' : 'ADD TASK'


  useEffect(() => {
    const valueSub = value.subscribe((value) => {
      setCurrent(value);
    });
    const currentSub = editItem.subscribe((value)=>{
      setCurrentEdit(value)
    })
    const editSub = editStatus.subscribe(status => setEditStatus(status))

    return () => {
      valueSub.unsubscribe();
      editSub.unsubscribe()
      currentSub.unsubscribe()
    };
  }, []);

  const changeHandler = (event) => {
    value.next(event.target.value);
    setCurrentEdit(event.target.value)
  };


  return (
    <>
            <TextField
            variant="outlined"
            value={editStatus.value ? currentEdit : current}
            onChange={changeHandler}
            label={labeInput}
          />
    </>
  );
};
export default InputField;
